const express =require('express');


const bodyParser = require('body-parser');
const movieRoutes = require('./src/routes/movies/moviesRoutes');
const directorRoutes = require('./src/routes/director/directorRoutes')

const app = express()
const port = 3000;

app.use(bodyParser.json());

app.get('/', (req, res) =>{
    res.send("welcome to api testing 😀");
});

app.use("/api/movies", movieRoutes);
app.use("/api/directors", directorRoutes)

app.listen(port, () => console.log(`app listening at port ${port}`));