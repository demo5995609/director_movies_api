const Joi = require("joi");

// Rank validation
function rankValid(rank) {
  const schemaRank = Joi.object({
    rank: Joi.number().integer().positive().required(),
  });
  var { error, value } = schemaRank.validate({ rank: rank });
  if (error) {
    return { message: error.details[0].message.replace(/"/g, "") };
  }
  return null;
}

//Title validation
function titleValid(title) {
  const schemaTitle = Joi.object({
    title: Joi.string().min(5).required(),
  });
  var { error, value } = schemaTitle.validate({ title: title });
  if (error) {
    return { message: error.details[0].message.replace(/"/g, "") };
  }
  return null;
}

//Description validation
function descriptionValid(description) {
  const schemaDescription = Joi.object({
    description: Joi.string().min(10).required(),
  });
  var { error, value } = schemaDescription.validate({
    description: description,
  });
  if (error) {
    return { message: error.details[0].message.replace(/"/g, "") };
  }
  return null;
}

//runtime validation
function runtimeValid(runtime) {
  const schemaRuntime = Joi.object({
    runtime: Joi.number().integer().positive().required(),
  });
  var { error, value } = schemaRuntime.validate({ runtime: runtime });
  if (error) {
    return { message: error.details[0].message.replace(/"/g, "") };
  }
  return null;
}

//Genre validation
function genreValid(genre) {
  const schemaGenre = Joi.object({
    genre: Joi.string().min(3).required(),
  });
  const { error, value } = schemaGenre.validate({ genre: genre });

  if (error) {
    return { message: error.details[0].message.replace(/"/g, "") };
  }
  return null;
}

//Rating validation
function ratingValid(rating) {
  const schemaRating = Joi.object({
    rating: Joi.number().required(),
  });

  const { error, value } = schemaRating.validate({ rating: rating });
  if (error) {
    return { message: error.details[0].message.replace(/"/g, "") };
  }
  return null;
}

//metaScore validation
function metaScoreValid(metaScore) {
  const schemaMetaScore = Joi.object({
    metaScore: Joi.number().integer().required(),
  });

  const { error, value } = schemaMetaScore.validate({ metaScore: metaScore });
  if (error) {
    return { message: error.details[0].message.replace(/"/g, "") };
  }
  return null;
}

//Votes validation
function votesValid(votes) {
  const schemaVotes = Joi.object({
    votes: Joi.number().integer().required(),
  });

  const { error, value } = schemaVotes.validate({ votes: votes });
  if (error) {
    return { message: error.details[0].message.replace(/"/g, "") };
  }
  return null;
}

//gross_earning validation
function grossEarningValid(grossEarning) {
  const schemaGrossEarning = Joi.object({
    grossEarning: Joi.number().required(),
  });

  const { error, value } = schemaGrossEarning.validate({
    grossEarning: grossEarning,
  });
  if (error) {
    return { message: error.details[0].message.replace(/"/g, "") };
  }
  return null;
}

//Director Name validate
function directorNameValid(directorName) {
  const schemaDirectorName = Joi.object({
    directorName: Joi.string().min(3).required(),
  });
  const { error, value } = schemaDirectorName.validate({
    directorName: directorName,
  });

  if (error) {
    return { message: error.details[0].message.replace(/"/g, "") };
  }
  return null;
}

//Actor Name validate
function actorValid(actor) {
  const schemaActor = Joi.object({
    actor: Joi.string().min(3).required(),
  });
  const { error, value } = schemaActor.validate({
    actor: actor,
  });

  if (error) {
    return { message: error.details[0].message.replace(/"/g, "") };
  }
  return null;
}

//Released Year validation
function releasedYearValid(releasedYear) {
  const schemaReleasedYear = Joi.object({
    releasedYear: Joi.number().integer().required(),
  });

  const { error, value } = schemaReleasedYear.validate({
    releasedYear: releasedYear,
  });
  if (error) {
    return { message: error.details[0].message.replace(/"/g, "") };
  }
  return null;
}

//
module.exports = {
  rankValid,
  titleValid,
  descriptionValid,
  runtimeValid,
  genreValid,
  ratingValid,
  metaScoreValid,
  votesValid,
  grossEarningValid,
  directorNameValid,
  actorValid,
  releasedYearValid,
};
