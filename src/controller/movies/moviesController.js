const pool = require("../../db/db");
const queries = require("../../queries/movies/moviesQueries");
const directorQuery = require("../../queries/director/directorQueries");
const Joi = require("joi");
const {
  runtimeValid,
  descriptionValid,
  titleValid,
  rankValid,
  genreValid,
  ratingValid,
  metaScoreValid,
  votesValid,
  grossEarningValid,
  directorNameValid,
  actorValid,
  releasedYearValid,
} = require("../inputValidation/moviesValidation");

const getAllMovies = (req, res) => {
  pool.query(queries.getAllMovies, (error, result) => {
    if (error) {
      throw error;
    } else {
      res.status(200).json(result.rows);
    }
  });
};

//Get Movie by Id
const getMovieById = (req, res) => {
  const movieId = req.params.id;

  //validate id
  const schema = Joi.object({
    id: Joi.number().integer().positive().required(),
  });
  const { error, value } = schema.validate({ id: movieId });
  if (error) {
    res
      .status(400)
      .json({ message: error.details[0].message.replace(/"/g, "") });
  } else {
    pool.query(queries.getMovieById, [movieId], (error, result) => {
      if (error) {
        throw error;
      } else {
        res.status(200).json(result.rows);
      }
    });
  }
};

//Adding new Movie
const addMovie = (req, res) => {
  const {
    rank,
    title,
    description,
    runtime,
    genre,
    rating,
    metascore,
    votes,
    gross_earning_in_mil,
    director_name,
    actor,
    release_year,
  } = req.body;

  if (rankValid(rank)) {
    res.status(400).json(rankValid(rank));
  } else if (titleValid(title)) {
    res.status(400).json(titleValid(title));
  } else if (descriptionValid(description)) {
    res.status(400).json(descriptionValid(description));
  } else if (runtimeValid(runtime)) {
    res.status(400).json(runtimeValid(runtime));
  } else if (genreValid(genre)) {
    res.status(400).json(genreValid(genre));
  } else if (ratingValid(rating)) {
    res.status(400).json(ratingValid(rating));
  } else if (metaScoreValid(metascore)) {
    res.status(400).json(metaScoreValid(metascore));
  } else if (votesValid(votes)) {
    res.status(400).json(votesValid(votes));
  } else if (grossEarningValid(gross_earning_in_mil)) {
    res.status(400).json(grossEarningValid(gross_earning_in_mil));
  } else if (directorNameValid(director_name)) {
    res.status(400).json(directorNameValid(director_name));
  } else if (actorValid(actor)) {
    res.status(400).json(actorValid(actor));
  } else if (releasedYearValid(release_year)) {
    res.status(400).json(releasedYearValid(release_year));
  } else {
    //checking rank exist
    pool.query(queries.rankExist, [rank], (error, result) => {
      if (error) {
        throw error;
      } else if (result.rows.length > 0) {
        res.status(400).json({ message: "Rank already Exist! 🙁" });
      }
    });

    //check director is new or presented
    pool.query(
      directorQuery.directorNameExist,
      [director_name],
      (error, result) => {
        // console.log(result);
        if (error) {
          throw error;
        } else if (result.rows.length > 0) {
          //if present ==> take the id from director database
          const directorOldId = result.rows[0].s.split(",")[0].substring(1);
          pool.query(
            queries.addMovie,
            [
              rank,
              title,
              description,
              runtime,
              genre,
              rating,
              metascore,
              votes,
              gross_earning_in_mil,
              director_name,
              actor,
              release_year,
              directorOldId,
            ],
            (error, result) => {
              if (error) {
                throw error;
              } else {
                res.status(200).json({ message: "movie added 😀" });
              }
            }
          );
        } else {
          //if not ==> create new director in director database and get id from there
          pool.query(
            directorQuery.addDirector,
            [director_name],
            (error, result) => {
              if (error) {
                throw error;
              } else {
                // getting id of director after creation
                pool.query(
                  directorQuery.directorIdByName,
                  [director_name],
                  (error, result) => {
                    if (error) {
                      throw error;
                    } else {
                      const newDirectorId = result.rows[0].id;
                      pool.query(
                        queries.addMovie,
                        [
                          rank,
                          title,
                          description,
                          runtime,
                          genre,
                          rating,
                          metascore,
                          votes,
                          gross_earning_in_mil,
                          director_name,
                          actor,
                          release_year,
                          newDirectorId,
                        ],
                        (error, result) => {
                          if (error) {
                            throw error;
                          } else {
                            res.status(200).json({ message: "movie added 😀" });
                          }
                        }
                      );
                    }
                  }
                );
              }
            }
          );
        }
      }
    );
  }
  //
};

// Delete Movie by id
const deleteMovieById = (req, res) => {
  const id = req.params.id;
  pool.query(queries.movieIdExist, [id], (error, result) => {
    if (error) {
      throw error;
    } else if (result.rows.length == 0) {
      res.status(400).json({ message: "movie id not exist! 🙁" });
    } else {
      pool.query(queries.deleteMovie, [id], (error, result) => {
        if (error) {
          throw error;
        } else {
          res.status(200).json({ message: "Movie deleted Succesfully! 😊" });
        }
      });
    }
  });
};

const updateMovie = (req, res) => {
  const id = req.params.id;

  pool.query(queries.movieIdExist, [id], (error, result) => {
    if (error) {
      throw error;
    } else if (result.rows.length == 0) {
      res.status(400).json({ message: "id not exist! 🙁" });
    }
  });

  const {
    rank,
    title,
    description,
    runtime,
    genre,
    rating,
    metascore,
    votes,
    gross_earning_in_mil,
    director_name,
    actor,
    release_year,
  } = req.body;

  if (rankValid(rank)) {
    res.status(400).json(rankValid(rank));
  } else if (titleValid(title)) {
    res.status(400).json(titleValid(title));
  } else if (descriptionValid(description)) {
    res.status(400).json(descriptionValid(description));
  } else if (runtimeValid(runtime)) {
    res.status(400).json(runtimeValid(runtime));
  } else if (genreValid(genre)) {
    res.status(400).json(genreValid(genre));
  } else if (ratingValid(rating)) {
    res.status(400).json(ratingValid(rating));
  } else if (metaScoreValid(metascore)) {
    res.status(400).json(metaScoreValid(metascore));
  } else if (votesValid(votes)) {
    res.status(400).json(votesValid(votes));
  } else if (grossEarningValid(gross_earning_in_mil)) {
    res.status(400).json(grossEarningValid(gross_earning_in_mil));
  } else if (directorNameValid(director_name)) {
    res.status(400).json(directorNameValid(director_name));
  } else if (actorValid(actor)) {
    res.status(400).json(actorValid(actor));
  } else if (releasedYearValid(release_year)) {
    res.status(400).json(releasedYearValid(release_year));
  } else {
    //checking rank exist
    pool.query(queries.rankExist, [rank], (error, result) => {
      if (error) {
        throw error;
      } else if (result.rows.length > 0) {
        res.status(400).json({ message: "Rank already Exist! 🙁" });
      } else {
        //check director is new or presented
        pool.query(
          directorQuery.directorNameExist,
          [director_name],
          (error, result) => {
            // console.log(result);
            if (error) {
              throw error;
            } else if (result.rows.length > 0) {
              //if present ==> take the id from director database
              let director_Id = result.rows[0].s.split(",")[0].substring(1);
              pool.query(
                queries.updateMovie,
                [
                  rank,
                  title,
                  description,
                  runtime,
                  genre,
                  rating,
                  metascore,
                  votes,
                  gross_earning_in_mil,
                  director_name,
                  actor,
                  release_year,
                  director_Id,
                  id,
                ],
                (error, result) => {
                  if (error) {
                    throw error;
                  } else {
                    res.status(200).json({ message: "movie updated 😀" });
                  }
                }
              );
            } else {
              //if not ==> create new director in director database and get id from there
              pool.query(
                directorQuery.addDirector,
                [director_name],
                (error, result) => {
                  if (error) {
                    throw error;
                  } else {
                    // getting id of director after creation
                    pool.query(
                      directorQuery.directorIdByName,
                      [director_name],
                      (error, result) => {
                        if (error) {
                          throw error;
                        } else {
                          let director_id = result.rows[0].id;
                          pool.query(
                            queries.updateMovie,
                            [
                              rank,
                              title,
                              description,
                              runtime,
                              genre,
                              rating,
                              metascore,
                              votes,
                              gross_earning_in_mil,
                              director_name,
                              actor,
                              release_year,
                              director_id,
                              id,
                            ],
                            (error, result) => {
                              if (error) {
                                throw error;
                              } else {
                                res
                                  .status(200)
                                  .json({ message: "movie updated 😀" });
                              }
                            }
                          );
                        }
                      }
                    );
                  }
                }
              );
            }
          }
        );
      }
    });
  }
};

module.exports = {
  getAllMovies,
  getMovieById,
  addMovie,
  deleteMovieById,
  updateMovie,
};
