const pool = require("../../db/db");
const queries = require("../../queries/director/directorQueries");
const Joi = require("joi");

//Getting all Directors
const getAllDirectors = (req, res) => {
  pool.query(queries.getAllDirectors, (error, result) => {
    if (error) {
      throw error;
    } else {
      res.status(200).json(result.rows);
    }
  });
};

//Getting Driector by it's id
const getDirectorById = (req, res) => {
  // validation schema
  const schema = Joi.object({
    id: Joi.number().integer().positive().required(),
  });
  const { error, value } = schema.validate({ id: req.params.id });

  if (error) {
    res
      .status(400)
      .json({ message: error.details[0].message.replace(/"/g, "") });
  } else {
    pool.query(queries.getDirectorById, [value.id], (error, result) => {
      if (error) {
        throw error;
      } else {
        res.status(200).json(result.rows);
      }
    });
  }
};

//Adding a Director
const addDirector = (req, res) => {
  //validate sent name it should be more than 2 chars
  const schema = Joi.object({
    director_name: Joi.string().min(3).required(),
  });
  const { error, value } = schema.validate({ director_name: req.body.director_name });

  if (error) {
    res
      .status(400)
      .json({ message: error.details[0].message.replace(/"/g, "") });
  } else {
    pool.query(queries.directorNameExist, [value.director_name], (error, result) => {
      if (error) {
        throw error;
      } else if (result.rows.length) {
        return res
          .status(400)
          .json({ message: "Director name already exists! 🫣" });
      } else {
        pool.query(queries.addDirector, [value.director_name], (error, result) => {
          if (error) {
            throw error;
          } else {
            res.status(200).json({ message: "Added Successfully!" });
          }
        });
      }
    });
  }
};

//delete director by director id
const deleteDirectorById = (req, res) => {
  // validation schema
  const schema = Joi.object({
    id: Joi.number().integer().positive().required(),
  });
  const { error, value } = schema.validate({ id: req.params.id });

  if (error) {
    res
      .status(400)
      .json({ message: error.details[0].message.replace(/"/g, "") });
  } else {
    pool.query(queries.directorIdExist, [value.id], (error, result) => {
      if (error) {
        throw error;
      } else if (result.rows.length === 0) {
        res.status(400).json({ message: "Director id not exist!" });
      } else {
        pool.query(queries.deleteDirectorById, [value.id], (error, result) => {
          if (error) {
            throw error;
          } else {
            res.status(200).json({ message: "Director deleted 😀" });
          }
        });
      }
    });
  }
};

//update director by id
const updateDirectorById = (req, res) => {
  const directorId = req.params.id;
  const directorName = req.body.name;
  // validation schema
  const schemaId = Joi.object({
    id: Joi.number().integer().positive().required(),
  });
  const { error, valueId } = schemaId.validate({ id: directorId });

  if (error) {
    res
      .status(400)
      .json({ message: error.details[0].message.replace(/"/g, "") });
  } else {
    pool.query(queries.directorIdExist, [directorId], (error, result) => {
      if (error) {
        throw error;
      } else if (result.rows.length === 0) {
        res.status(400).json({ message: "Director id not exist!" });
      } else {
        //validate sent name it should be more than 2 chars
        const schemaName = Joi.object({
          name: Joi.string().min(3).required(),
        });
        const { error, valueName } = schemaName.validate({
          name: directorName,
        });

        if (error) {
          res
            .status(400)
            .json({ message: error.details[0].message.replace(/"/g, "") });
        } else {
          pool.query(
            queries.updateDirector,
            [directorName, directorId],
            (error, result) => {
              if (error) {
                throw error;
              } else {
                console.log("working...");
                res.status(200).json({ message: "Succesfully updated! 😎" });
              }
            }
          );
        }
      }
    });
  }
};

module.exports = {
  getAllDirectors,
  getDirectorById,
  addDirector,
  deleteDirectorById,
  updateDirectorById,
};
