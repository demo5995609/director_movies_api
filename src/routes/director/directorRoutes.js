const { Router } = require("express");
const {
  getAllDirectors,
  getDirectorById,
  addDirector,
  deleteDirectorById,
  updateDirectorById,
} = require("../../controller/director/directorCotroller");

const router = Router();

router.get("/", getAllDirectors);
router.post("/", addDirector);
router.put("/:id", updateDirectorById)
router.get("/:id", getDirectorById);
router.delete("/:id", deleteDirectorById);

module.exports = router;
