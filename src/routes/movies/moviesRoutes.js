const { Router } = require("express");
const {
  getAllMovies,
  getMovieById,
  addMovie,
  deleteMovieById,
  updateMovie,
} = require("../../controller/movies/moviesController");

const router = Router();

router.get("/", getAllMovies);
router.post("/", addMovie);
router.get("/:id", getMovieById);
router.delete("/:id", deleteMovieById)
router.put("/:id", updateMovie)

module.exports = router;
