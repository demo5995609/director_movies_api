const fs = require("fs").promises;
const path = require("path");
const pool = require("./db/db");
const queries = require("./queries/queries");

async function insertDirectors() {
  try {
    const responseData = JSON.parse(
      await fs.readFile("src/data/movies.json", "utf-8")
    );

    let directorData = responseData.reduce((acc, data) => {
      acc[data["Director"]] = data["Director"];
      return acc;
    }, {});
    directorData = Object.values(directorData);

    directorData.forEach((director) => {
      pool.query(queries.insertDirectors, [director], (error, result) => {
        if (error) {
          throw error;
        } else {
          console.log("director inserted succesfully!");
        }
      });
    });
  } catch (error) {
    console.log(error);
  }
}

// insertDirectors();

async function insertMovies() {
  try {
    const responseData = JSON.parse(
      await fs.readFile("src/data/movies.json", "utf-8")
    );
    responseData.forEach((movie) =>{
      console.log(movie);
      pool.query(queries.getDirectorIdByName, [movie.Director], (error, director) =>{
        const directorId = director.rows[0].id;
        movie.Metascore = movie.Metascore === "NA" ? 0 : movie.Metascore;
        movie.Gross_Earning_in_Mil = movie.Gross_Earning_in_Mil === "NA" ? 0 : movie.Gross_Earning_in_Mil;
        if(error){
          throw error;
        }else{
          pool.query(queries.insertMovies, [movie.Rank, movie.Title, movie.Description, movie.Runtime, movie.Genre, movie.Rating, movie.Metascore, movie.Votes, movie.Gross_Earning_in_Mil, movie.Director, movie.Actor, movie.Year, directorId], (error, result) =>{
            if(error){
              throw error;
            }else{
              console.log("Movie inserted Successfully!");
            }
          })
        }
      })
    })
  } catch (error) {}
}

insertMovies();