const insertDirectors = "insert into directors (director_name) values($1)";
const insertMovies =
  "insert into movies (rank, title, description, runtime, genre, rating, metascore, votes, gross_earning_in_mil, director_name, actor, release_year, director_id) values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)";
const getDirectorIdByName = "select id from directors where director_name = $1";

module.exports = {
  insertDirectors,
  insertMovies,
  getDirectorIdByName,
};
