const getAllMovies = "select * from movies";
const getMovieById = "select * from movies where id = $1";
const addMovie =
  "insert into movies (rank, title, description, runtime, genre, rating, metascore, votes, gross_earning_in_mil, director_name, actor, release_year, director_id) values($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)";
const rankExist = "select s from movies s where s.rank = $1";
const deleteMovie = "delete from movies where id = $1";
const movieIdExist = "select s from movies s where s.id = $1";
const updateMovie = "update movies set rank = $1, title = $2, description = $3, runtime = $4, genre = $5, rating = $6, metascore = $7, votes = $8, gross_earning_in_mil = $9, director_name = $10, actor = $11, release_year = $12, director_Id = $13 where id = $14";

module.exports = {
  getAllMovies,
  getMovieById,
  addMovie,
  rankExist,
  deleteMovie,
  movieIdExist,
  updateMovie,
};

