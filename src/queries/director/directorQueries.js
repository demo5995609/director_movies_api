const getAllDirectors = "select * from directors";
const getDirectorById = "select * from directors where id = $1";
const addDirector = "insert into directors (director_name) values($1)";
const directorNameExist =
  "select s from directors s where s.director_name = $1";
const directorIdByName = "select id from directors where director_name = $1";
const deleteDirectorById = "delete from directors where id = $1";
const directorIdExist = "select s from directors s where s.id = $1";
const updateDirector = "update directors set director_name = $1 where id = $2";

module.exports = {
  getAllDirectors,
  getDirectorById,
  addDirector,
  directorNameExist,
  directorIdByName,
  deleteDirectorById,
  directorIdExist,
  updateDirector,
};
